var WiFi = {
	parent: require('Wifi'),

	init: function() {
		this.parent.setSNTP(Config.ntp.server, Config.ntp.timezone);
		this.parent.getAPIP(function(details) {
			WiFi.parent.setHostname('esp' + (details.mac.split(':').join('')).substr(6, 6));
		});

		this.parent.on('connected', function(details) {
			require('http').createServer(onPageRequest).listen(Config.http.port);
			console.log('WebServer listening on ' + details.ip + ':' + Config.http.port);
		});

		this.parent.on('disconnected', function(details) {
			console.log(details);
			WiFi.connect();
		});

		this.init = function(){};

		return this;
	},

	connect: function() {
		console.log('Connecting to WiFi: ' + Config.wifi.ssid);

		this.parent.connect(Config.wifi.ssid, {password: Config.wifi.password}, function(error) {
			if(error !== null) {
				console.log(error);
				WiFi.connect();
			}
		});
	},
};
module.exports = WiFi;