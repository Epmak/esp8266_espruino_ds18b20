var js = `var Ajax = {
	xhr: null,
	request: function (url, method, data, success, failure) {
		if (!this.xhr) {
			this.xhr = window.ActiveX ? new ActiveXObject('Microsoft.XMLHTTP'): new XMLHttpRequest();
		}
		this.xhr.onload = success || function(){};
		this.xhr.onerror = failure || function(){};
		this.xhr.open(method, url, true);
		this.xhr.send(data);
	},
};

var Update = {
	canvas: null,
	canvas2d: null,
	current_temp: null,
	title: null,

	init: function() {
		this.canvas = document.getElementById('canvas');
		this.canvas2d = this.canvas.getContext('2d');
		this.current_temp = document.getElementById('current_temp');
		this.title = document.title;
		return this;
	},

	preRequest: function() {
		setTimeout(Update.request, 1000);
	},

	request: function() {
		Ajax.request('/temp_all.json', 'GET', null, Update.process, Update.error);
	},

	process: function() {
		var d = JSON.parse(this.responseText).all;
		var temp = Math.round(d[d.length - 1] * 100) / 100 + '°C';

		document.title = Update.title + ' ' + temp;
		Update.current_temp.innerHTML = temp;

		Update.canvas.width = d.length * 10;
		Update.canvas.height = 300;
		Update.canvas2d.clearRect(0, 0, Update.canvas.width, Update.canvas.height);
		var width = Update.canvas.width;
		var height = Update.canvas.height / 10 / 2;
		Update.canvas2d.moveTo(0, 150 - (d[0] - d[d.length - 1]) * height);
			for (i in d) {
			var x = i * width / (d.length - 1);
			var y = height * 10 - (d[i] - d[d.length - 1]) * height;
			Update.canvas2d.lineTo(x, y);
			if(i % 5 == 1) {
				Update.canvas2d.fillText(Math.round(d[i] * 100) / 100, x, y - 2);
			}
		}
		Update.canvas2d.stroke();

		Update.preRequest();
	},

	error: function() {
		document.title = Update.title + this.statusText;
		Update.preRequest();
	},
};

Update.init().request();`;

var index = `<!DOCTYPE html>
<html>
	<head>
		<title>Temp</title>
	</head>
	<body>
		<div id="current_temp"></div>
		<canvas id="canvas" width="0" height="0" style="border:1px solid #888;"></canvas>
		<script type="text/javascript" src="/main.js"></script>
	</body>
</html>`;

module.exports.js = js;
module.exports.index = index;
