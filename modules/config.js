var Config = {
	wifi: {
		ssid: 'wifi',
		password: 'password'
	},

	http: {
		port: 80
	},

	ntp: {
		server: 'ru.pool.ntp.org',
		timezone: 5
	}
};
module.exports = Config;
