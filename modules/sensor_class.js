var Sensor = {
	power_pin: NodeMCU.D5,
	sensor_pin: NodeMCU.D6,

	parent: null,
	status: false,
	history: new Float32Array(60),

	init: function() {
		this.on();
		let ow = new OneWire(this.sensor_pin);
		this.parent = require('DS18B20').connect(ow);
		this.off();

		setInterval(this.timer, 1000);

		this.init = function(){};
	},

	on: function() {
		if(this.status === true) {
			return false;
		}
		this.power_pin.write(true); // Включить сенсор
		this.status = true;
	},

	off: function() {
		if(this.status === false) {
			return false;
		}
		this.power_pin.write(false); // Выключить сенсор
		this.status = false;
	},

	timer: function() {
		Sensor.on();
		Sensor.parent.getTemp(Sensor.push);
	},

	push: function(temp) {
		if(temp === null) {
			return false;
		}

		//console.log(temp);
		// move history back
		for (let i = 1; i < Sensor.history.length; i++)
			Sensor.history[i - 1] = Sensor.history[i];

		// insert new history at end
		Sensor.history[Sensor.history.length - 1] = temp;

		Sensor.off();
	},
};
module.exports = Sensor;
