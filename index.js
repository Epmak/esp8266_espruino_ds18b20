var Pages = require('pages_class'),
	Config = require('config'),
	Sensor = require('sensor_class'),
	WiFi = require('wifi_class');

function onPageRequest(req, res) {
	//console.log(req);

	let temp = Math.round(Sensor.history[Sensor.history.length - 1] * 100) / 100;
	let contentType = 'text/html';
	let content = null;
	let code = 200;

	switch(req.url) {
		case '/temp_current.json':
			contentType = 'application/json';
			content = JSON.stringify({current: temp});
			break;

		case '/temp_all.json':
			contentType = 'application/json';
			content = JSON.stringify({all: Sensor.history});
			break;

		case '/main.js':
			contentType = 'application/javascript';
			content = Pages.js;
			break;

		default:
			contentType = 'text/html';
			content = Pages.index;
	}

	res.writeHead(code, {'Content-Type': contentType});
	//res.write(content);
	res.end(content);
}

E.inited = false;
E.on('init', function() {
	//E.setPassword('pass');
	//E.lockConsole();
	setDeepSleep(1);

	WiFi.init().connect();
	Sensor.init();

	E.inited = true;

	setTimeout(function() {
		console.log(process.memory());
	}, 5000);
});

if(!E.inited) {
	save();
}
